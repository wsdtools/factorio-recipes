
buildMenu();

$('#reset-all-btn').click(function(e) {
	e.preventDefault();
	if(!confirm('This will remove all position and display options. Are you sure?')) return;
	savedData.positions = {};
	savedData.endNodesLeft = {};
	savedData.endNodesRight = {};
	savedData.save();
	graphGenerator.reRender();
});