
$.fn.dragableSvg = function(config) {
	
	const wrap = this.eq(0);
	
	config = $.extend({
		zoom: null
	}, config);
	
	if(!config.zoom)
	{
		if(wrap.attr('viewBox'))
			config.zoom = parseFloat(wrap.width()) / parseFloat(wrap.attr('viewBox').split(' ')[2]);
		else
			config.zoom = 1;
	}
	
	const gPosition = function(g) {
		let t = $(g).attr('transform');
		if(!t) {
			console.warn("getting transform of invalid object", g);
			return [];
		}
		return t
			.split('translate(').pop()
			.split(')').shift()
			.split(',').map(parseFloat);
	};
	
	let draggingObject = null;
	let dragPositon = false;
	let dragStartPositon = false;
	wrap.on('mousedown', '.draggable-handle', (e) => {
		e.stopPropagation();
		e.stopImmediatePropagation();
		draggingObject = $(e.target).parents('.draggable').eq(0);
		let [x, y] = gPosition(draggingObject);
		dragStartPositon = {
			x: e.offsetX,
			y: e.offsetY,
			initX: x,
			initY: y,
		};
		draggingObject.trigger('start-drag');
	})
	.mousemove((e) => {
		e.stopPropagation();
		e.stopImmediatePropagation();
		if(!draggingObject) return;
		let dx = e.offsetX - dragStartPositon.x;
		let dy = e.offsetY - dragStartPositon.y;
		dx /= config.zoom;
		dy /= config.zoom;
		let x = dragStartPositon.initX + dx;
		let y = dragStartPositon.initY + dy;
		draggingObject.attr('transform', "translate("+x+","+y+")")
		draggingObject.trigger('drag', {
			initX: dragStartPositon.initX,
			initY: dragStartPositon.initY,
			x: x,
			y: y,
		});
	})
	.mouseup((e) => {
		e.stopPropagation();
		e.stopImmediatePropagation();
		if(!draggingObject) return;
		
		// console.log('draggingObject', draggingObject);
		
		draggingObject.trigger('end-drag');
		draggingObject = false;
	});
	// @todo: mouseout
	
	return this;
};