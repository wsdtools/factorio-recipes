
const SCHEMEWIDTH = 800;
const SCHEMEHEIGHT = 800;

let savedData = {
	positions: {},
	endNodesLeft: {},
	endNodesRight: {},
	save: function() {
		localStorage.setItem('FactorioRecepieData', JSON.stringify({
			positions: this.positions,
			endNodesLeft: this.endNodesLeft,
			endNodesRight: this.endNodesRight,
		}));
	},
	load: function() {
		this.positions = {};
		this.endNodesLeft = {};
		this.endNodesRight = {};
		let json = localStorage.getItem('FactorioRecepieData');
		if(!json) return;
		let data = JSON.parse(json);
		if(!data) return;
		if(data.positions) this.positions = data.positions;
		if(data.endNodesLeft) this.endNodesLeft = data.endNodesLeft;
		if(data.endNodesRight) this.endNodesRight = data.endNodesRight;
	},
};
savedData.load();

let savedPositions = {}; // @todo: load from page storage

const parseSvgString = function(s) {
	var div = document.createElementNS('http://www.w3.org/1999/xhtml', 'div');
	div.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg">'+s+'</svg>';
	var frag = document.createDocumentFragment();
	while(div.firstChild.firstChild)
		frag.appendChild(div.firstChild.firstChild);
	return $(frag).children();
};

const graphGenerator = {

	SCHEMEWIDTH: 800,
	SCHEMEHEIGHT: 800,

	currentRecepyCode: null,
	currentItemCode: null,

	links: [],
	positions: {},
	endNodesLeft: [],
	endNodesRight: [],	
	saveData: function()
	{
		savedData.positions[this.currentRecepyCode] = this.positions;
		savedData.endNodesLeft[this.currentRecepyCode] = this.endNodesLeft;
		savedData.endNodesRight[this.currentRecepyCode] = this.endNodesRight;
		savedData.save();
	},
	loadData: function() {
		this.positions = savedData.positions[this.currentRecepyCode];
		this.endNodesLeft = savedData.endNodesLeft[this.currentRecepyCode];
		this.endNodesRight = savedData.endNodesRight[this.currentRecepyCode];
		if(!this.positions) this.positions = {};
		if(!this.endNodesLeft) this.endNodesLeft = [];
		if(!this.endNodesRight) this.endNodesRight = [];
	},
	
	categoryIconItem: {
		"crafting": "assembling-machine-1",
		"oil-processing": "oil-refinery",
		"chemistry": "chemical-plant",
		"crafting-with-fluid": "assembling-machine-2",
		"smelting": "stone-furnace",
		"advanced-crafting": "assembling-machine-3",
		"centrifuging": "centrifuge",
		"rocket-building": "rocket-silo"
	},

	resetLinks: function() { this.links = []; },
	
	addLink: function(link) {
		let index = null;
		for(let i = 0; i < this.links.length; i++)
		{
			if(
				this.links[i].from == link.from
				&&
				this.links[i].to == link.to
			)
			{
				index = i;
				break;
			}
		}
		if(index === null)
			this.links.push(link);
		// else
			// this.links[index].amount += link.amount;
	},
	
	generateBackGraphRec: function(targetRecepyCode, targetCode, resultAmount)
	{
		// список игнорируемых узлов
		if(this.endNodesLeft.indexOf(targetRecepyCode) != -1) return;
		
		const targetRecepy = DATA.recipes[targetRecepyCode];
		for(let ing of targetRecepy.ingredients)
		{
			this.addLink({
				from: ing.name, 
				to: targetCode,
				type: 'ingredient',
				amount: ing.amount, // количество, нужное для производства результата (в количестве по рецепту),
				resultAmount: resultAmount, // количество, которое в итоге получится по рецепту
				category: targetRecepy.category,
				energy_required: targetRecepy.energy_required,
			});
			
			let recepies = []; // список всех рецептов, которые делают этот ингридиент
			for(let r_code in DATA.recipes)
			{
				const r = DATA.recipes[r_code];
				
				let match = false;
				for(let res of r.results)
				{
					if(res.name == ing.name)
					{
						match = true;
						break;
					}
				}
				if(match) recepies.push(r);
			}
			
			if(recepies.length == 1)
			{
				let resultingTargetAmount = 0;
				for(let res of recepies[0].results)
				{
					if(res.name == ing.name)
					{
						resultingTargetAmount = res.amount;
						break;
					}
				}
				if(!resultingTargetAmount) continue;
				this.generateBackGraphRec(recepies[0].name, ing.name, resultingTargetAmount);
			}
			else
			{
				if(this.endNodesLeft.indexOf(ing.name) != -1) continue;
				
				for(let r of recepies)
				{
					let amount = 0;
					for(let res of r.results)
					{
						if(res.name == ing.name)
						{
							amount = res.amount;
							break;
						}
					}
					
					if(!amount) continue;
					
					this.addLink({
						from: r.name, 
						to: ing.name,
						type: 'recepy-variant',
						amount: 1,
						resultAmount: amount,
						category: r.category,
						energy_required: r.energy_required,
					});
				}
			}
		}
	},

	generateFrontGraphRec: function(sourceItemCode, passedNodes)
	{
		if(passedNodes.indexOf(sourceItemCode) != -1) return; // для избежание зацикливания
		passedNodes.push(sourceItemCode);
		
		// список игнорируемых узлов
		if(this.endNodesRight.indexOf(sourceItemCode) != -1) return;
		
		for(let r_code in DATA.recipes)
		{
			const r = DATA.recipes[r_code];
			let amount = 0;
			for(let ing of r.ingredients)
			{
				if(ing.name == sourceItemCode)
				{
					amount = ing.amount;
					break;
				}
			}
			if(!amount) continue;
			
			if(r.results.length == 1)
			{
				this.addLink({
					from: sourceItemCode,
					to: r.results[0].name,
					type: 'ingredient-of',
					amount: amount,
					resultAmount: r.results[0].amount,
					category: r.category,
					energy_required: r.energy_required,
				});
				
				this.generateFrontGraphRec(r.results[0].name, passedNodes);
			}
			else
			{
				this.addLink({
					from: sourceItemCode,
					to: r.name,
					type: 'used-in-recepy',
					amount: amount,
					resultAmount: 1,
					category: r.category,
					energy_required: r.energy_required,
				});
				
				for(let res of r.results)
				{
					this.addLink({
						from: r.name,
						to: res.name,
						type: 'ingredient-of',
						amount: 1,
						resultAmount: res.amount,
						category: r.category,
						energy_required: 0,
					});
					
					this.generateFrontGraphRec(res.name, passedNodes);
				}
			}
		}
	},

	breakGraphLoops: function()
	{
		let there_were_loops
		do
		{
			there_were_loops = false;
			for(let i = 0; i < this.links.length; i++)
			{
				let loopLink = this.findGraphLoopsRec(this.links[i].to, []);
				if(!loopLink) continue;
				for(let j = 0; j < this.links.length; j++)
				{
					if(this.links[j].from == loopLink.from && this.links[j].to == loopLink.to)
					{
						this.links[j].to = 'loop';
						there_were_loops = true;
						break;
					}
				}
			}
		}
		while(there_were_loops);
	},
	
	findGraphLoopsRec: function(code, passedLinks)
	{
		for(let link of this.links)
		{
			if(link.from != code) continue;
			let c = `${link.from}/${link.to}`;
			if(passedLinks.indexOf(c) != -1) return link;
			let ret = this.findGraphLoopsRec(link.to, passedLinks.concat([c]));
			if(ret) return ret;
		}
		return false;
	},

	generateGraph: function(initialRecepyCode, initialItemCode)
	{
		this.resetLinks();
		
		if(initialRecepyCode)
		{
			const recepy = DATA.recipes[initialRecepyCode];
			
			if(recepy.results.length == 1)
			{
				this.generateBackGraphRec(initialRecepyCode, initialItemCode, recepy.results[0].amount);
			}
			else
			{	
				let amount = 1;
				for(let r of recepy.results)
				{
					if(r.name == initialItemCode) amount = r.amount;
					this.addLink({
						from: initialRecepyCode,
						to: r.name,
						type: (r.name == initialItemCode) ? 'result' : 'additional-result',
						amount: 1,
						resultAmount: r.amount,
						category: recepy.category,
						energy_required: recepy.energy_required,
					});
				}
				this.generateBackGraphRec(initialRecepyCode, initialRecepyCode, amount);
			}
		}
		
		this.generateFrontGraphRec(initialItemCode, []);
	},
	
	generateNodes: function() {
		
		let froms = [];
		let tos = [];
		for(let i = 0; i < this.links.length; i++)
		{
			froms.push(this.links[i].from);
			tos.push(this.links[i].to);
		}
		
		let codes = froms.concat(tos).filter(function(value, index, self) { return self.indexOf(value) === index; });
		
		// generate dummy values
		this.nodes = {};
		for(let code of codes)
		{
			this.nodes[code] = {level: null, amount: 0};
		}
		
		for(let link of this.links)
		{
			this.nodes[link.from].amount += link.totalAmount;
		}
		
		for(let link of this.links)
		{
			if(this.nodes[link.to].amount) continue;
			this.nodes[link.to].amount = link.resultAmount;
		}
		
		let level0 = codes.filter(function(value) { return tos.indexOf(value) == -1; })
		
		for(let code of level0)
		{
			this.nodes[code].level = 0;
			this.setNodesLevelsRec(code, 1, []);
		}
	},
	
	setNodesLevelsRec: function(code, level, passedNodes)
	{
		if(passedNodes.indexOf(code) != -1) return; // для избежание зацикливания
		passedNodes.push(code);
		
		for(let i = 0; i < this.links.length; i++)
		{
			if(this.links[i].from != code) continue;
			let to = this.links[i].to;
			if(this.nodes[to].level)
				this.nodes[to].level = Math.max(this.nodes[to].level, level);
			else
				this.nodes[to].level = level;
			this.setNodesLevelsRec(to, level+1, passedNodes);
		}
	},
	
	countAmountTotals: function(initialRecepyCode)
	{
		this.countAmountTotalsLeftRec(initialRecepyCode, 1);
		this.countAmountTotalsRightRec(initialRecepyCode, 1);
	},
	countAmountTotalsLeftRec: function(parentCode, parentAmount)
	{
		for(let link of this.links)
		{
			if(link.to != parentCode) continue;
			if(!link.totalAmount) link.totalAmount = 0;
			link.totalAmount += parentAmount * link.amount / link.resultAmount;
			this.countAmountTotalsLeftRec(link.from, link.totalAmount);
		}
	},
	countAmountTotalsRightRec: function(parentCode, parentAmount)
	{
		for(let link of this.links)
		{
			if(link.from != parentCode) continue;
			if(!link.totalAmount) link.totalAmount = 0;
			link.totalAmount += parentAmount * link.amount / link.resultAmount;
			this.countAmountTotalsRightRec(link.to, link.totalAmount);
		}
	},
	
	reRender: function()
	{
		this.render(this.currentRecepyCode, this.currentItemCode);
	},
	
	render: function(initialRecepyCode, initialItemCode)
	{
		const _this = this;
		
		this.currentRecepyCode = initialRecepyCode;
		this.currentItemCode = initialItemCode;
		
		this.loadData();
		
		this.generateGraph(initialRecepyCode, initialItemCode);
		
		this.breakGraphLoops();
		
		this.countAmountTotals(initialItemCode);
		
		this.generateNodes();
		
		if(!this.nodes || Object.keys(this.nodes).length < 1)
		{
			this.nodes[initialItemCode] = {level: 1, amount: 1};
		}
		this.nodes[initialItemCode].amount = null;
		
		let mainLevel = this.nodes[initialItemCode].level;
		for(let code in this.nodes)
		{
			if(this.nodes[code].level > mainLevel) this.nodes[code].type = 'right';
			else if(this.nodes[code].level < mainLevel) this.nodes[code].type = 'left';
			else this.nodes[code].type = 'main';
		}
		
		if(this.positions)
			$('#reset-items-position').show();
		else
			$('#reset-items-position').hide();
		
		$('#reset-items-position')
			.off('click')
			.on('click', function() {
				_this.positions = {};
				_this.saveData();
				_this.reRender();
			});
		
		this.renderNodes();
		this.renderLinks();
		this.setDragEvents();
		
		return [this.links, this.nodes];
	},
	
	displayItemIcon: function(input)
	{
		let item = null;
		
		if(typeof input == 'string') item = DATA.items[input];
		else if(typeof input == 'object') item = input;
		
		if(!item)
		{
			console.warn('Item not found 1: ', input);
			return $(null);
		}
		
		return parseSvgString('<svg width="30" height="30" version="1.1">'
			+'<image x="0px" y="0px" width="512" height="480" xlink:href="icons.png" />'
			+'</svg>')
			.attr('viewBox', '' + (30 + 30 * item.icon_col) + ' ' + (30 * item.icon_row) + ' 30 30');
	},

	
	renderNodes: function()
	{
		const _this = this;
		
		$('#items').children().remove();
		this.nodeTags = {};
		
		let max_level = 0;
		for(let code in this.nodes) max_level = Math.max(max_level, this.nodes[code].level);
		
		for(let x = 0; x <= max_level; x++)
		{
			let y = 0;
			let level_nodes = [];
			for(let code in this.nodes)
			{
				if(this.nodes[code].level == x)
					level_nodes.push(code);
			}
			
			for(let code of level_nodes)
			{
				let px, py;
				if(this.positions[code])
				{
					px = this.positions[code][0];
					py = this.positions[code][1];
				}
				else
				{
					px = (x + 1) * this.SCHEMEWIDTH / (max_level + 2) - 35;
					py = (y + 1) * this.SCHEMEHEIGHT / (level_nodes.length + 1) - 70;
					this.positions[code] = [px, py];
				}
				
				let item = DATA.items[code];
				let type = 'item';
				if(!item)
				{
					item = DATA.recipes[code];
					type = 'recepy';
				}
				
				if(!item)
				{
					console.warn('Item not found 2: ', code);
					continue;
				}
				
				let g = parseSvgString('<g class="item draggable"/>');
				g.attr('transform', 'translate('+px+','+py+')').appendTo('#items');
				g.data('code', code);
				
				this.nodeTags[code] = g;
				
				let circle = parseSvgString('<circle class="frame"/>')
					.appendTo(g)
					.attr('cx', 35)
					.attr('cy', 35)
					.attr('r', 35)
					.attr('fill', 'none')
					.attr('stroke', 'grey');
					
				if(this.nodes[code].type == 'main')
				{
					circle.addClass('main');
					circle.attr('r', 50);
				}
					
				this.displayItemIcon(item)
					.addClass('draggable-handle')
					.attr('x', 20)
					.attr('y', 10)
					.appendTo(g);
				
				if(['left', 'main'].indexOf(this.nodes[code].type) != -1)
				{
					parseSvgString('<text class="control-x"/>')
						.text((_this.endNodesLeft.indexOf(code) == -1) ? '<' : '+')
						.attr('x', 5)
						.attr('y', 35)
						.appendTo(g)
						// .text(this.nodes[code].type)
						.click(function() {
							let index = _this.endNodesLeft.indexOf(code);
							if(index == -1)
							{
								_this.endNodesLeft.push(code);
							}
							else
							{
								_this.endNodesLeft.splice(index, 1);
							}
							_this.saveData();
							_this.reRender();
						});
				}
				
				if(['right', 'main'].indexOf(this.nodes[code].type) != -1)
				{
					parseSvgString('<text class="control-x"/>')
						.text((_this.endNodesRight.indexOf(code) == -1) ? '>' : '+')
						.attr('x', 55)
						.attr('y', 35)
						.appendTo(g)
						.click(function() {
							let index = _this.endNodesRight.indexOf(code);
							if(index == -1)
							{
								_this.endNodesRight.push(code);
							}
							else
							{
								_this.endNodesRight.splice(index, 1);
							}
							_this.saveData();
							_this.reRender();
						});
				}

				parseSvgString('<text class="node-title"/>')
					.css('text-anchor', 'middle')
					.attr('x', 35)
					.attr('y', 60)
					.text(item.localized_name.en)
					.appendTo(g);
				
				if(this.nodes[code].amount)
					parseSvgString('<text class="node-amount"/>')
						.css('text-anchor', 'middle')
						.attr('x', 60)
						.attr('y', 20)
						.text(parseFloat(parseFloat(this.nodes[code].amount).toFixed(2)))
						.appendTo(g);
					
				y++;
			}
		}
	},
	
	
	countArrowD: function(p1, p2, old_d)
	{	
		let x1, x2, y1, y2, d;
		
		if((!p1 || !p2) && !old_d)
		{
			console.warn('can not byuild the path: ', p1, p2, old_d);
			return '';
		}
		
		if(!p1 || !p2)
		{
			d = old_d.split(' ').map(parseFloat);
		}
		
		if(p1)
		{
			x1 = p1[0] + 35;
			y1 = p1[1] + 35;
		}
		else
		{
			x1 = d[1];
			y1 = d[2];
		}
		
		if(p2)
		{
			x2 = p2[0] + 35;
			y2 = p2[1] + 35;
		}
		else
		{
			x2 = d[8];
			y2 = d[9];
		}
		
		let dy = 0.3 * (x2 - x1);
		let m1 = [x1, y1-dy];
		let m2 = [x2, y2-dy];
		
		return `M ${x1} ${y1} C ${m1[0]} ${m1[1]} ${m2[0]} ${m2[1]} ${x2} ${y2}`;
	},
	
	countArrowHeadD: function(path, pos)
	{
		let length = pos * path.get(0).getTotalLength();
		let p1 = path.get(0).getPointAtLength(length + 5);
		let p2 = path.get(0).getPointAtLength(length - 5);
		let x = p2.x - p1.x;
		let y = p2.y - p1.y;
		let r = Math.exp(0.5 * Math.log(x*x + y*y));
		
		let a = Math.atan2(x, y);
		let x1 = p1.x + (r * Math.sin(a + 0.4));
		let y1 = p1.y + (r * Math.cos(a + 0.4));
		let x2 = p1.x + (r * Math.sin(a - 0.4));
		let y2 = p1.y + (r * Math.cos(a - 0.4));
		
		return `M ${p1.x.toFixed(4)} ${p1.y.toFixed(4)} L ${x1.toFixed(4)} ${y1.toFixed(4)} ${x2.toFixed(4)} ${y2.toFixed(4)} Z`;
	},
	
	countArrowLabelGroupPosition: function(arrow)
	{
		let middle = arrow.get(0).getPointAtLength(arrow.get(0).getTotalLength() / 2);
		return `translate(${middle.x}, ${middle.y})`;
	},

	renderLinks: function()
	{
		$('#arrows').children().remove();
		
		this.arrows = {
			begin: {},
			end: {},
		};
		for(let link of this.links)
		{
			let p1 = this.positions[link.from];
			let p2 = this.positions[link.to];
			
			let arrow = parseSvgString('<path class="arrow"/>');
					
			arrow
				.appendTo('#arrows')
				.attr('d', this.countArrowD(p1, p2));
			
			let a1 = parseSvgString('<path class="arrow-head"/>')
				.attr('d', this.countArrowHeadD(arrow, 0.33))
				.appendTo('#arrows');
				
			let a2 = parseSvgString('<path class="arrow-head"/>')
				.attr('d', this.countArrowHeadD(arrow, 0.66))
				.appendTo('#arrows');
			
			let g = parseSvgString('<g/>')
				.attr('transform', this.countArrowLabelGroupPosition(arrow))
				.appendTo('#arrows');
			
			parseSvgString('<circle fill="white" stroke="gray"/>')
				.appendTo(g)
				.attr('r', 15)
				.attr('cx', 0)
				.attr('cy', 0);
			
			if(link.totalAmount)
				parseSvgString('<text class="qty-display"/>')
					.appendTo(g)
					.attr('x', 0)
					.attr('y', 17)
					.text(parseFloat(link.totalAmount.toFixed(2)));
					
			if(link.energy_required)
				parseSvgString('<text class="energy-display"/>')
					.appendTo(g)
					.attr('x', 10)
					.attr('y', -5)
					.text(parseFloat(link.energy_required.toFixed(2)));
			
			// выводить категорию крафта
			this.displayItemIcon(this.categoryIconItem[link.category])
				.appendTo(g)
				.attr('width', 17)
				.attr('height', 17)
				.attr('x', -17)
				.attr('y', -17);
				
			if(!this.arrows.begin[link.from]) this.arrows.begin[link.from] = [];
			this.arrows.begin[link.from].push({
				path: arrow,
				labels: g,
				arrows: [a1, a2],
			});
			if(!this.arrows.end[link.to]) this.arrows.end[link.to] = [];
			this.arrows.end[link.to].push({
				path: arrow,
				labels: g,
				arrows: [a1, a2],
			});
		}
	},
	
	setDragEvents: function()
	{
		const _this = this;
		
		$('svg#recepy-display')
			.off('drag')
			.off('start-drag')
			.off('end-drag')
			.on('drag', '.draggable', function(event, pos) {
				
				let code = $(this).data('code');
				if(!pos) return;
				let x = pos.x;
				let y = pos.y;
				
				_this.positions[code] = [x, y];
				
				if(_this.arrows.begin[code])
				{
					for(let arrow of _this.arrows.begin[code])
					{
						arrow.path.attr('d', _this.countArrowD([x, y], null, arrow.path.attr('d')));
						arrow.labels.attr('transform', _this.countArrowLabelGroupPosition(arrow.path));
						arrow.arrows[0].attr('d', _this.countArrowHeadD(arrow.path, 0.33));
						arrow.arrows[1].attr('d', _this.countArrowHeadD(arrow.path, 0.66));
					}
				}
				if(_this.arrows.end[code])
				{
					for(let arrow of _this.arrows.end[code])
					{
						arrow.path.attr('d', _this.countArrowD(null, [x, y], arrow.path.attr('d')));
						arrow.labels.attr('transform', _this.countArrowLabelGroupPosition(arrow.path));
						arrow.arrows[0].attr('d', _this.countArrowHeadD(arrow.path, 0.33));
						arrow.arrows[1].attr('d', _this.countArrowHeadD(arrow.path, 0.66));
					}
				}
			})
			.on('end-drag', '.draggable', function(event, pos) {
				
				$('#reset-items-position').show();
				
				// сохранить новое положение
				_this.saveData();
				// _this.reRender();

			});

	}
};

$('svg#recepy-display').dragableSvg();
