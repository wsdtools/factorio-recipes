
const buildMenu = function() {
	
	let data = {};
	for(let code in DATA.recipes)
	{
		let r = DATA.recipes[code];
		if(!data[r.subgroup]) data[r.subgroup] = {};
		for(let result of r.results)
		{
			if(!data[r.subgroup][result.name]) data[r.subgroup][result.name] = [];
			data[r.subgroup][result.name].push(r);
		}
	}
	
	$('#nav-subgroups').text('');
	$('#nav-items').text('');
	$('#nav-recipes').text('');
	
	let li = $('<li class="nav-item">').appendTo('#nav-subgroups');
	$('<a class="nav-link">')
		.appendTo(li)
		.text('[resources]')
		.click(function(e) {
			e.stopPropagation();
			$('#nav-subgroups .active').removeClass('active');
			$(this).addClass('active');
			$('#nav-items').text('');
			$('#nav-recipes').text('');
			for(let i in DATA.resource)
			{
				let item = DATA.resource[i];
				let icon = $('<span class="f-icon">')
					.css('background-position-x', -1 * item.icon_col * 32)
					.css('background-position-y', -1 * item.icon_row * 32);
					
				let li = $('<li class="nav-item">').appendTo('#nav-items');
				$('<a class="nav-link">')
					.appendTo(li)
					.text('['+i+']')
					.prepend(icon)
					.click(function(e) {
						e.stopPropagation();
						$('#nav-items .active').removeClass('active');
						$(this).addClass('active');
						graphGenerator.render(false, i);
					});
			}
				
		});
	
	for(let s in data)
	{
		let li = $('<li class="nav-item">').appendTo('#nav-subgroups');
		$('<a class="nav-link">')
			.appendTo(li)
			.text('['+s+']')
			.click(function(e) {
				e.stopPropagation();
				$('#nav-subgroups .active').removeClass('active');
				$(this).addClass('active');
				$('#nav-items').text('');
				$('#nav-recipes').text('');
				for(let i in data[s])
				{
					let item = DATA.items[i];
					let icon = $('<span class="f-icon">')
						.css('background-position-x', -1 * item.icon_col * 32)
						.css('background-position-y', -1 * item.icon_row * 32);
					
					let li = $('<li class="nav-item">').appendTo('#nav-items');
					$('<a class="nav-link">')
						.appendTo(li)
						.text('['+i+']')
						.prepend(icon)
						.click(function(e) {
							e.stopPropagation();
							$('#nav-items .active').removeClass('active');
							$(this).addClass('active');
							$('#nav-recipes').text('');
							for(let r of data[s][i])
							{
								let li = $('<li class="nav-item">').appendTo('#nav-recipes');
								$('<a class="nav-link">')
									.appendTo(li)
									.text('['+r.name+']')
									.click(function(e) {
										e.stopPropagation();
										$('#nav-recipes .active').removeClass('active');
										$(this).addClass('active');
										graphGenerator.render(r.name, i);
									});
							}
							// $('#nav-recipes a:eq(0)').click();
						});
					
					$('#nav-items a:eq(0)').click();
				}
			});
	}
	$('#nav-subgroups a:eq(0)').click();
};